﻿namespace MarioObjects.Objects.GameObjects.Command
{
    public interface IShootFireBall
    {
        void Execute(int x, int y);
    }
}
