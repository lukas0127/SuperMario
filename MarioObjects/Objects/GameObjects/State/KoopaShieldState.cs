﻿using System;

namespace MarioObjects.Objects.GameObjects.State
{
    public class KoopaShieldState : IKoopaState
    {
        MonsterKoopa monster;

        public KoopaShieldState(MonsterKoopa monster)
        {
            this.monster = monster;
        }

        public void OnAnimate(object sender, EventArgs e)
        {
            monster.ReturningTime++;

            if (monster.ReturningTime > 20)
            {
                monster.KoopaSetState(monster.GetReturnState());
            }
        }

        public void OnWalk(object sender, EventArgs e)
        {

        }

        public void SetProperties()
        {
            monster.width = 16;
            monster.height = 27;
            monster.ReturningTime = 0;
            monster.OffsetIndex = 0;
            monster.ImageIndex = 4;
            monster.Animated = false;
        }
    }
}
