﻿using static MarioObjects.Objects.GameObjects.Mario;

namespace MarioObjects.Objects.GameObjects.Memento
{
    public class MarioTypeMemento
    {
        private MarioType type;

        public MarioTypeMemento(MarioType type)
        {
            this.type = type;
        }

        public MarioType GetMarioType()
        {
            return type;
        }
    }
}
