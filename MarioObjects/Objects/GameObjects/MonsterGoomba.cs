﻿using System;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.Utils;
using MarioObjects.Objects.GameObjects.Strategy;

namespace MarioObjects.Objects.GameObjects
{
    public class MonsterGoomba : MoveableAnimatedObject
    {
        public Boolean FallDie;

        public static LevelEditorObject GetLEObject()
        {
            return new LevelEditorObject(16, 16, 4, 1, ObjectType.OT_Goomba, null);
        }

        public static MonsterGoomba SetLEObject(LevelEditorObject le)
        {
            return new MonsterGoomba(le.x, le.y);
        }

        public void GoombaFallDie()
        {
            if (FallDie == false)
            {
                FallDie = true;

            }
        }
        public void GoombaDie()
        {
            Animated = false;
            Live = false;
        }
        public override void OnWalk(object sender, EventArgs e)
        {
            if (!FallDie)
                base.OnWalk(sender, e);
            else
            {
                Animated = false;
                ImageIndex = 3;
                newy += 3;

                if (newy >= LevelGenerator.CurrentLevel.height)
                {
                    Visible = false;
                }

            }


        }
        public MonsterGoomba(int x, int y)
            : base(OT: ObjectType.OT_Goomba,strategy: new GoombaStrategy())
        {
            AnimatedCount = 2;
            this.x = x;
            this.y = y;
            SetWidthHeight();
            FallDie = false;

            TimerGenerator.AddTimerEventHandler(TimerType.TT_50, OnWalk);
            TimerGenerator.AddTimerEventHandler(TimerType.TT_100, OnAnimate);
        }
        public override void Draw()
        {
            base.Draw();
        }
        public override void OnAnimate(object sender, EventArgs e)
        {
            if (Visible)
            {
                if (Live)
                    base.OnAnimate(sender, e);
                else
                {
                    if (ImageIndex != 2)
                        ImageIndex = 2; //Die Picture
                    else
                    {
                        Visible = false; //Next Time,Visible False
                    }
                }
            }
        }
    }

}
