﻿using MarioObjects.Objects.BaseObjects;
using static MarioObjects.Objects.GameObjects.MonsterPiranah;

namespace MarioObjects.Objects.GameObjects.Strategy
{
    class PiranahStrategy : IIntersectionStrategy
    {
        public void CustomIntersection(Collision c, GraphicObject g, AnimatedGraphicObject obj)
        {
            switch (g.OT)
            {
                case ObjectType.OT_Mario:
                    {
                        // Handle collision with Mario
                        if (((MonsterPiranah)obj).Move != PiranaMove.PM_None) // Only if the pirana is out of its pipe
                        {
                            Mario m = (Mario)g;
                            if (!m.Blinking)
                                if (m.Type == Mario.MarioType.MT_Big || m.Type == Mario.MarioType.MT_Fire)
                                {
                                    m.Type = Mario.MarioType.MT_Small;
                                    m.StartBlinking();
                                    m.SetMarioProperties();
                                }
                        }
                    }
                    break;
            }
        }
    }
}
