﻿namespace MarioObjects.Objects.BaseObjects
{
    public abstract class AbstractBuilder : IBuilder
    {

        protected AbstractBuildable item;

        public void build()
        {
            
        }

        public AbstractBuildable getBuildable()
        {
            return item;
        }

        protected void AddObject(GraphicObject g)
        {
            item.AddObject(g);
        }
    }
}
