﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;
using MarioObjects.Objects.Patterns;

namespace MarioObjects.Objects.BaseObjects
{
    public class Level : AbstractLevel
    {

        public Level()
        {
            LevelGenerator.CurrentLevel = this;

            width = 1024;
            height = 464;//464
            Objects = new List<GraphicObject>();
            LevelBitmap = ImageGenerator.GetImage(ObjectType.OT_BG_Block);
            Visitor_Check_Object_Enabled = new VisitorCheckObjectEnabled();

            MainColision = new Collision(new Rectangle(0, 0, 0, 0), new Rectangle(0, 0, 0, 0), CollisionType.CT_Moveable, CollisionDirection.CD_ButtomLeft);
            AvailableRect = new Rectangle(0, 0, 0, 0);
            ButtomLeft = new Point(0, 0);
            ButtomRight = new Point(0, 0);
            TopRight = new Point(0, 0);
            TopLeft = new Point(0, 0);

            SRC = new Rectangle(0, 0, 0, 0);
            DEST = new Rectangle(0, 0, 0, 0);



            //MarioObject = new Mario();
            //Objects.Add(MarioObject);

            IntersectsEvents = new List<Intersection>();
        }

        public void DrawBackground(Rectangle Rect)
        {
            Graphics xGraph;
            //xGraph = Graphics.FromImage(Screen.GetScreen);
            xGraph = Screen.Instance.Background.xGraph;
            //Rectangle dest = new Rectangle(0, 0, Screen.Width, Screen.Height);
            //Rectangle src = new Rectangle(Screen.OutputScreen.x / 3, (LevelBitmap.Height - Screen.Height) - Screen.OutputScreen.y / 3, Screen.Width, Screen.Height);
            //Rectangle dest = new Rectangle(x - Screen.BackgroundScreen.x, y - (LevelGenerator.LevelHeight - Screen.BackgroundScreen.height) + Screen.BackgroundScreen.y, width, height);




            int Y = (LevelGenerator.CurrentLevel.LevelBitmap.Height - Screen.OutputScreen.y / 3);
            Y = Y - (Y - (Rect.Y));
            int X = (Screen.OutputScreen.x / 3) + (Rect.X - Screen.BackgroundScreen.x);

            DEST.X = Rect.X - Screen.BackgroundScreen.x;
            DEST.Y = Rect.Y - (LevelGenerator.LevelHeight - Screen.BackgroundScreen.height) + Screen.BackgroundScreen.y;
            DEST.Width = Rect.Width;
            DEST.Height = Rect.Height;

            SRC.X = X;
            SRC.Y = Y;
            SRC.Width = Rect.Width;
            SRC.Height = Rect.Height;
            //xGraph.DrawImage(LevelBitmap, dest, src, GraphicsUnit.Pixel);
            xGraph.DrawImage(LevelBitmap, DEST, SRC, GraphicsUnit.Pixel);

            //xGraph.Dispose();


        }
    }
}
