﻿using System;
using System.Drawing;
using MarioObjects.Objects.Utils;
using MarioObjects.Objects.GameObjects.Strategy;

namespace MarioObjects.Objects.BaseObjects
{
    public class AnimatedGraphicObject : StaticGraphicObject, IAnimatedGraphicObject
    {
        public int AnimatedCount;
        public Boolean Animated;
        private IIntersectionStrategy strategy;

        public virtual void OnAnimate(Object sender, EventArgs e)
        {
            if (Animated == true)
            {
                ImageIndex++;
                if (ImageIndex >= AnimatedCount)
                    ImageIndex = 0;

                ObjectChangedDrawFlag = true;

            }
        }

        public IIntersectionStrategy GetStrategy()
        {
            return strategy;
        }

        public override void Intersection(Collision c, GraphicObject g)
        {
            if (strategy != null)
                strategy.CustomIntersection(c, g, this);
        }

        public override void Draw()
        {
            base.Draw();

        }
        public AnimatedGraphicObject(ObjectType Type, IIntersectionStrategy strategy)
        {
            this.strategy = strategy;
            Animated = true;
            OT = Type;
            Bitmap b = ImageGenerator.GetImage(OT);
            if (b != null)
                ImageCount = (int)Math.Round((double)b.Width / b.Height);
        }

    }

}
