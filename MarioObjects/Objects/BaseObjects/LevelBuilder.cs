﻿using System.Collections.Generic;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;
namespace MarioObjects.Objects.BaseObjects
{
    class  LevelBuilder : ILevelBuilder
    {
        private Level level;
        private string LevelXmlSchema = "lev1.xml";
        public LevelBuilder()
        {
            level = new Level();
        }
        public Level getLevel()
        {
            return level;
        }
        protected void AddObject(GraphicObject g)
        {
            if (g.Objects != null)
                for (int i = 0; i < g.Objects.Count; i++)
                    AddObject(g.Objects[i]);

            level.AddObject(g);
        }
        public void buildLevel()
        {
            List<LevelEditorObject> list = MarioEditorXML.Load_From_XML(LevelXmlSchema);
            Mario MTemp = null;
            foreach (LevelEditorObject le in list)
            {
                Factory f = new Factory();
                GraphicObject g = f.SetEditorObject(le);
                if (g != null && g.OT != ObjectType.OT_Mario)
                    level.AddObject(g);
                else if (g.OT == ObjectType.OT_Mario)
                    MTemp = (Mario)g;

            }

            level.AddObject(MTemp);

            for (int i = 0; i < level.Objects.Count; i++)
                if (level.Objects[i].OT == ObjectType.OT_Mario)
                {
                    level.MarioObject = (Mario)level.Objects[i];
                    break;
                }
        }
    }
}
