﻿using System.Drawing;
using MarioObjects.Objects.Patterns;

namespace MarioObjects.Objects.BaseObjects
{
    public interface IGraphicObject
    {
        void AcceptVisitor(VisitorObject V);
        void AddCollision(Intersection IC);
        void AddObject(GraphicObject g);
        void CheckObjectEnabled();
        Rectangle GetObjectRect();
        void Intersection(Collision c, GraphicObject g);
        void Intersection_None();
        void LogMe(Collision c, GraphicObject g);
        void SetObjectChangeFlag(bool F);
        void SetWidthHeight();
    }
}