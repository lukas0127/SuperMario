﻿namespace MarioObjects.Objects.BaseObjects
{
    public interface IStaticGraphicObject : IGraphicObject
    {
        void Draw();
    }
}