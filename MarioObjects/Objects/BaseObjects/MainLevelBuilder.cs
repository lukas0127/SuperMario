﻿using System;
using System.Collections.Generic;
using System.Text;
using Helper;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects;
using MarioObjects.Objects.Utils;

namespace MarioObjects.Objects.BaseObjects
{
    class MainLevelBuilder: AbstractLevelBuilder
    {

        private string LevelXmlSchema = "lev1.xml";

        public MainLevelBuilder()
        {
            base.setLevelXmlSchema(LevelXmlSchema);
            item = new Level();
        }
    }
}
