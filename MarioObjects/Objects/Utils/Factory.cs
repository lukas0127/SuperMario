﻿using MarioObjects.Objects.BaseObjects;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace MarioObjects.Objects.Utils
{
    class Factory
    {
        public GraphicObject SetEditorObject(LevelEditorObject le)
        {
            Assembly asm = Assembly.GetExecutingAssembly();

            Type objType = Type.GetType("MarioObjects.Objects.GameObjects." + le.name);
            MethodInfo i = objType.GetMethod("SetLEObject");//pakeist
            GraphicObject g = null;

            if (i != null)
            {
                g = (GraphicObject)i.Invoke(null, new object[] { le });
            }

            return g;
        }
    }
}
