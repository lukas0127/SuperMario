using System;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Helper;
using MarioObjects.Objects.BaseObjects;
using MarioObjects.Objects.GameObjects;

namespace MarioObjects
{
    public partial class frmMain : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;
        private const int WM_PAINT = 0x000F;


        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd,
                         int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("User32.dll")]
        private static extern short GetAsyncKeyState(System.Windows.Forms.Keys vKey); 

        public Level lev;
        public int BackPaint = 0;
        Rectangle SRC ;
        Rectangle DEST;

        DateTime MyTime;


        public frmMain()
        {
            InitializeComponent();
        }

        public void Init_Properties()
        {

            Width = 320;
            Height = 240  + 25;

            Cursor.Hide();
            pMain.Image = new Bitmap(320,240);
            pMain.Left = 0;
            pMain.Top = 0;
            pMain.Width = pMain.Image.Width;
            pMain.Height = pMain.Image.Height;


            Left = SystemInformation.PrimaryMonitorSize.Width / 2 - this.Width / 2;
            Top = SystemInformation.PrimaryMonitorSize.Height / 2 - this.Height / 2;

            Media.PlaySound(Media.SoundType.ST_level2);
        }
        public void OnTest(object sender, EventArgs e)
        { 
        
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            MyTime = DateTime.Now;

            SRC = new Rectangle(0, 0, 320, 240);
            DEST = new Rectangle(0, 0, 320, 240);

            Init_Properties();

            MainLevelBuilder levelBuilder = new MainLevelBuilder();
            LevelEngineer levelEngineer = new LevelEngineer(levelBuilder);
            levelEngineer.makeLevel();
            lev = levelEngineer.getLevel();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
        }

        protected override void OnPaint(PaintEventArgs e)
        {
         }
        private void timerPaint_Tick(object sender, EventArgs e)
        {

            pMain.Invalidate();
            DateTime TimeClose = DateTime.Now;

            TimeSpan Diff = TimeClose.Subtract(MyTime);

            this.Text = string.Format("{0:00}:{1:00}:{2:00}", Diff.Hours, Diff.Minutes, Diff.Seconds);

        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            Boolean KeyRight = false;
            Boolean KeyLeft = false;

            int state = Convert.ToInt32(GetAsyncKeyState(Keys.Right).ToString());
            KeyRight = state == -32767;
            state = Convert.ToInt32(GetAsyncKeyState(Keys.Left).ToString());
            KeyLeft = state == -32767;


            if ((e.Modifiers & Keys.Control) == Keys.Control)
                lev.MarioObject.StartJump(false,0);

            if (e.KeyValue == (int)Keys.Right || KeyRight)
                lev.MarioObject.MarioMove(Mario.MarioMoveState.J_Right);

            if (e.KeyValue == (int)Keys.Left || KeyLeft)
                lev.MarioObject.MarioMove(Mario.MarioMoveState.J_Left);

            if (e.KeyValue == (int)Keys.Space)
                lev.MarioObject.MarioFireBall();


            if (e.KeyValue == (int)Keys.Up)
                lev.MarioObject.UpPressed = true;

            if (e.KeyValue == (int)Keys.Escape)
                this.Close();
        }

        private void frmMain_KeyUp(object sender, KeyEventArgs e)
        {

            if (e.KeyValue == (int)Keys.ControlKey)
                lev.MarioObject.StopJump();

            if (e.KeyValue == (int)Keys.Right)
                lev.MarioObject.StopMove();


            if (e.KeyValue == (int)Keys.Left)
                lev.MarioObject.StopMove();

            if (e.KeyValue == (int)Keys.Up)
                lev.MarioObject.UpPressed = false;


        }

        private void frmMain_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }


        }

        private void pMain_MouseDown(object sender, MouseEventArgs e)
        {
            frmMain_MouseDown(sender, e);
        }

        private void pMain_Paint(object sender, PaintEventArgs e)
        {
            Graphics xGraph = e.Graphics;
            lev.Draw();
            MarioObjects.Objects.Utils.Screen.Instance.DrawOnGraphic(xGraph);
        }

        private void timerBack_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            DateTime TimeClose = DateTime.Now;
            TimeSpan Diff = TimeClose.Subtract(MyTime);
            Logger.Instance.Log_Method(Diff.ToString());
        }

    }

}