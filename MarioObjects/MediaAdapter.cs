﻿using MarioObjects.Objects.Utils;
using System;

namespace MarioObjects
{
    public class MediaAdapter : IVision
    {
        public Media media;

        public MediaAdapter(Media media)
        {
            this.media = media;
        }

        public void Draw_Output()
        {
            media.LoadFromResource();
        }

        public FMOD.Sound GetMedia(SoundType type)
        {
            return media.GetSound(type);
        }

        public void UpdateMedia(string name, ref FMOD.Sound s, Boolean Loop)
        {
            media.UpdateSound(name, ref s, Loop);
        }

        public void EraseMedia()
        {
            media.Destroy();
        }

        public void PlayMedia(SoundType type)
        {
            media.PlaySound(type);
        }

        public void PlayInnerMedia(SoundType type)
        {
            media.PlayInnerSound(type);
        }

    }
}
